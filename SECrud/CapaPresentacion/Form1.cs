﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CapaLogica.Login oLogin = new CapaLogica.Login();

            string select = oLogin.Muestra_Usuario(txtNickName.Text, txtClave.Text );
            /*
            switch (select)
            {

                case "Jhonny":
                    formEmpleados formEmpleados = new formEmpleados();
                    formEmpleados.Show();
                    this.Hide();
                    break;

            }*/
            if (select != "")
            {
                formEmpleados formEmpleados = new formEmpleados();
                formEmpleados.Show();
                this.Hide();
            }
            else {

                MessageBox.Show("Usuario o contraseña no validos");
                return;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
