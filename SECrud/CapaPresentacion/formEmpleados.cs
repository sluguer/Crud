﻿using System;
using CapaLogica;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion
{
    public partial class formEmpleados : Form
    {
        public formEmpleados()
        {
            InitializeComponent();
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void formEmpleados_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'eCrudDataSet.Empleados' Puede moverla o quitarla según sea necesario.
            this.empleadosTableAdapter.Fill(this.eCrudDataSet.Empleados);

        }

        private void button1_Click(object sender, EventArgs e)
        {

            Empleados oEmple = new Empleados();
            oEmple.insertarEmpleado(long.Parse(txtId.Text)  , txtNombre.Text, txtApellido.Text, txtNickName2.Text, txtClave2.Text);
            dataGridView1.DataSource = oEmple.Consultar_Todos_Empleados();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
