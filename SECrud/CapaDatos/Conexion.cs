﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaDatos
{
    public class Conexion
    {
        // cadena de conexion
        private string con = "Data Source=Sluguer;Initial Catalog=ECrud;Integrated Security=True";

        //encapsular la variable privada (con)// 
        public string Cnn
        {
            get { return con; }
            set { value = con; }
        }
        //crear la variable para la conexion con sql server 
        public SqlConnection conec;
   
       // conec = new SqlConnection(Cnn);

        //crear el metodo para la conexion 
        public SqlConnection conecte()
        {
            try
            {
                //instancia de la clase sqlconnection 
                conec = new SqlConnection(Cnn);
                conec.Open();
                return conec;

            }
            catch (SqlException ex)
            {
                MessageBox.Show("Error al conectar " + ex);
                conec.Close();
                return conec;
            }
        }




    }
}
