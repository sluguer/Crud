﻿using CapaDatos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaLogica
{
  public class Empleados
    {
////////Atributos////////////////// 
        private long id;
        private string nombre;
        private string apellido;
        private string NickName;
        private string Clave;
        public SqlDataAdapter Clt;
        public DataTable TEmpleado;

        ////////Metodos//////////////////
        public SqlCommand comando;
        Conexion oConexion = new Conexion();

        public long Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public string Apellido
        {
            get
            {
                return apellido;
            }

            set
            {
                apellido = value;
            }
        }

        public string NickName1
        {
            get
            {
                return NickName;
            }

            set
            {
                NickName = value;
            }
        }

        public string Clave1
        {
            get
            {
                return Clave;
            }

            set
            {
                Clave = value;
            }
        }


        public void insertarEmpleado(long id, string nombre, string apellido, string nickName, string Clave)
        {


            try
            {

                comando = new SqlCommand("insert into Empleados(id,nombre,apellido,nickName,clave) values ('" + id + "','" + nombre + "','" + apellido + "','" + nickName + "','" + Clave + "'); ", oConexion.conecte());

                comando.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Error al Insertar Datos" + ex);
            }
        }
              
            public DataTable Consultar_Todos_Empleados()
        {
            try
            {
                 Clt = new SqlDataAdapter("select * from Empleados",oConexion.conecte());
                TEmpleado = new DataTable();
                Clt.Fill(TEmpleado);
                return TEmpleado;

            }
            catch (SqlException ex)
            {
                MessageBox.Show("Error al cargar datos" + ex);
                return TEmpleado;
            }
        }




    } 
    }

