﻿using Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Usuarios
    {
        private int id;
        private string nombre;
        private string apellido;
        private int telefono;
        private string email;

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public string Apellido
        {
            get
            {
                return apellido;
            }

            set
            {
                apellido = value;
            }
        }

        public int Telefono
        {
            get
            {
                return telefono;
            }

            set
            {
                telefono = value;
            }
        }

        public string Email
        {
            get
            {
                return email;
            }

            set
            {
                email = value;
            }
        }
        public Usuarios()
        {

        }
        public Usuarios(int id, string nombre, string apellido,int telefono,string email)
        {

        }
        string sentencia;
            public void insertarUsuarios(int id, string nombre, string apellido, int telefono, string email)
        {
            Conexion oConex = new Conexion();
           sentencia = "exec insertUsuarios "+id+", '"+nombre+"', '"+apellido+"',"+telefono+",'"+email+"'";
            oConex.ejecutar_sentencias_sql(sentencia); 

        }
    }
}
